package com.ishank.webonisetest.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ishank Shukla on 03,April,2019
 */
public class Weatherdata {


    /**
     * message : accurate
     * cod : 200
     * count : 1
     * list : [{"id":1252736,"name":"Yerandwane","coord":{"lat":18.5,"lon":73.8333},"main":{"temp":311.267,"pressure":1006.86,"humidity":20,"temp_min":311.267,"temp_max":311.267,"sea_level":1006.86,"grnd_level":934.52},"dt":1554284081,"wind":{"speed":1.53,"deg":335.502},"sys":{"country":""},"rain":null,"snow":null,"clouds":{"all":0},"weather":[{"id":800,"main":"Clear","description":"Sky is Clear","icon":"01d"}]}]
     */

    private String message;
    private String cod;
    private int count;
    private ArrayList<ListBean> list;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<ListBean> getList() {
        return list;
    }

    public void setList(ArrayList<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * id : 1252736
         * name : Yerandwane
         * coord : {"lat":18.5,"lon":73.8333}
         * main : {"temp":311.267,"pressure":1006.86,"humidity":20,"temp_min":311.267,"temp_max":311.267,"sea_level":1006.86,"grnd_level":934.52}
         * dt : 1554284081
         * wind : {"speed":1.53,"deg":335.502}
         * sys : {"country":""}
         * rain : null
         * snow : null
         * clouds : {"all":0}
         * weather : [{"id":800,"main":"Clear","description":"Sky is Clear","icon":"01d"}]
         */

        private int id;
        private String name;
        private CoordBean coord;
        private MainBean main;
        private int dt;
        private WindBean wind;
        private SysBean sys;
        private Object rain;
        private Object snow;
        private CloudsBean clouds;
        private List<WeatherBean> weather;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public CoordBean getCoord() {
            return coord;
        }

        public void setCoord(CoordBean coord) {
            this.coord = coord;
        }

        public MainBean getMain() {
            return main;
        }

        public void setMain(MainBean main) {
            this.main = main;
        }

        public int getDt() {
            return dt;
        }

        public void setDt(int dt) {
            this.dt = dt;
        }

        public WindBean getWind() {
            return wind;
        }

        public void setWind(WindBean wind) {
            this.wind = wind;
        }

        public SysBean getSys() {
            return sys;
        }

        public void setSys(SysBean sys) {
            this.sys = sys;
        }

        public Object getRain() {
            return rain;
        }

        public void setRain(Object rain) {
            this.rain = rain;
        }

        public Object getSnow() {
            return snow;
        }

        public void setSnow(Object snow) {
            this.snow = snow;
        }

        public CloudsBean getClouds() {
            return clouds;
        }

        public void setClouds(CloudsBean clouds) {
            this.clouds = clouds;
        }

        public List<WeatherBean> getWeather() {
            return weather;
        }

        public void setWeather(List<WeatherBean> weather) {
            this.weather = weather;
        }

        public static class CoordBean {
            /**
             * lat : 18.5
             * lon : 73.8333
             */

            private double lat;
            private double lon;

            public double getLat() {
                return lat;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLon() {
                return lon;
            }

            public void setLon(double lon) {
                this.lon = lon;
            }
        }

        public static class MainBean {
            /**
             * temp : 311.267
             * pressure : 1006.86
             * humidity : 20
             * temp_min : 311.267
             * temp_max : 311.267
             * sea_level : 1006.86
             * grnd_level : 934.52
             */

            private double temp;
            private double pressure;
            private int humidity;
            private double temp_min;
            private double temp_max;
            private double sea_level;
            private double grnd_level;

            public double getTemp() {
                return temp;
            }

            public void setTemp(double temp) {
                this.temp = temp;
            }

            public double getPressure() {
                return pressure;
            }

            public void setPressure(double pressure) {
                this.pressure = pressure;
            }

            public int getHumidity() {
                return humidity;
            }

            public void setHumidity(int humidity) {
                this.humidity = humidity;
            }

            public double getTemp_min() {
                return temp_min;
            }

            public void setTemp_min(double temp_min) {
                this.temp_min = temp_min;
            }

            public double getTemp_max() {
                return temp_max;
            }

            public void setTemp_max(double temp_max) {
                this.temp_max = temp_max;
            }

            public double getSea_level() {
                return sea_level;
            }

            public void setSea_level(double sea_level) {
                this.sea_level = sea_level;
            }

            public double getGrnd_level() {
                return grnd_level;
            }

            public void setGrnd_level(double grnd_level) {
                this.grnd_level = grnd_level;
            }
        }

        public static class WindBean {
            /**
             * speed : 1.53
             * deg : 335.502
             */

            private double speed;
            private double deg;

            public double getSpeed() {
                return speed;
            }

            public void setSpeed(double speed) {
                this.speed = speed;
            }

            public double getDeg() {
                return deg;
            }

            public void setDeg(double deg) {
                this.deg = deg;
            }
        }

        public static class SysBean {
            /**
             * country :
             */

            private String country;

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }
        }

        public static class CloudsBean {
            /**
             * all : 0
             */

            private int all;

            public int getAll() {
                return all;
            }

            public void setAll(int all) {
                this.all = all;
            }
        }

        public static class WeatherBean {
            /**
             * id : 800
             * main : Clear
             * description : Sky is Clear
             * icon : 01d
             */

            private int id;
            private String main;
            private String description;
            private String icon;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMain() {
                return main;
            }

            public void setMain(String main) {
                this.main = main;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getIcon() {
                return icon;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }
        }
    }
}
