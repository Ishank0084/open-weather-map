package com.ishank.webonisetest.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ishank.webonisetest.R;
import com.ishank.webonisetest.model.Weatherdata;

import java.util.ArrayList;

/**
 * Created by Ishank Shukla on 03,April,2019
 */
public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.MyCustomViewHolder> {

    Context mContext;
    ArrayList<Weatherdata.ListBean> listBeans = new ArrayList<>();

    public WeatherListAdapter(Context context) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public WeatherListAdapter.MyCustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_list_item, null);
        WeatherListAdapter.MyCustomViewHolder viewHolder = new WeatherListAdapter.MyCustomViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull WeatherListAdapter.MyCustomViewHolder holder, int position) {
        if (listBeans.size() > 0) {

            if (!TextUtils.isEmpty(listBeans.get(position).getName())) {
                holder.tvCityName.setText(listBeans.get(position).getName());
            } else {

            }

            try {
                holder.tvTemp.setText("" + Math.round((listBeans.get(position).getMain().getTemp() - 273.15)));
            } catch (Exception e) {
                e.printStackTrace();
            }
            holder.tvUnitTemp.setText(".C");
            if (listBeans.get(position).getMain().getHumidity()!=0) {
             holder.tvHumidity.setText(String.valueOf(listBeans.get(position).getMain().getHumidity())+ " % Humidity");
            }


        }


    }

    public void setWeatherData(ArrayList<Weatherdata.ListBean> weatherDataList) {
        this.listBeans = weatherDataList;
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {
        return (null != listBeans ? listBeans.size() : 0);
    }

    public class MyCustomViewHolder extends RecyclerView.ViewHolder {
        TextView tvCityName;
        TextView tvTemp;
        TextView tvUnitTemp;
        TextView tvHumidity;

        public MyCustomViewHolder(View rootView) {
            super(rootView);
            tvCityName = (TextView) rootView.findViewById(R.id.tv_city_name);
            tvTemp = (TextView) rootView.findViewById(R.id.tv_temp);
            tvUnitTemp = (TextView) rootView.findViewById(R.id.tv_unit_temp);
         tvHumidity = (TextView) rootView.findViewById(R.id.tv_humidity);

        }
    }
}
